# Rhomb.io Api Integration

User manuals for integrating Rhomb.io Restful Api and WebSockets services into third party applications.

This documentation is written as a tutorial for developers and complements the [Api Reference](https://apidocs.m2msystemsource.com/) where all aspects are detailed in more depth.

For any questions:

jenguidanos@rhomb.io