# Rhombio RestFul Api Integration

[TOC]

## Introduction

Rhomb.io offers public access to its Api service for customers who wish to implement data integration into their platform. These are the key points of the Api:

* HTTP GET/POST/PUT/DELETE requests are made to access resources.
* HTTPS secure connections are always used.
* Authentication is done through HTTP Basic and all requests are required to include the access token in the header.

## Token de Acceso

We use the username and password of the service, separated with a colon, to obtain a string encoded in base 64. For example, if the username is `jonhdoe` and the password `123456` we can create the following string:

```text
jonhdoe:123456
```

We convert it to Base64 with an online service https://www.base64encode.org and get:

```text
am9uaGRvZToxMjM0NTY=
```

Our token is `am9uaGRvZToxMjM0NTY=` and we will add it to all requests made.

> IMPORTANT! This token gives full access to the platform data to edit or delete any information. You should not use it in browser applications intended for the general public. Instead you can use the "members" feature to add users with restricted permissions.

## First Request

We can perform the most basic example using the command line and cURL:

```bash
curl --user jonhdoe:123456 https://api.m2msystemsource.com/v1/accounts
-> [{"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}]
```

Same result with Base64 token

```bash
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/accounts
-> [{"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}]
```

### Javascript/jQuery example

```javascript
jQuery.ajax({
  type: 'GET',
  dataType: 'json',
  url: 'https://api.m2msystemsource.com/v1/accounts',
  headers: {
    Authorization: 'Basic am9uaGRvZToxMjM0NTY='
  }
  success: function(resp) {
	console.log(resp)
  },
  error: function() {

  }
});

/* // success result
[
  {
    "_id": "Hkm672j-z",
    "email": "info@jonhdoe.com",
    "username": "jonhdoe",
    "details": {
      "name": "",
      "company": "",
      "phone": ""
    }
  }
]
*/
```

### Ejemplo Python/Requests

```python
import requests

 url = 'https://api.m2msystemsource.com/v1/accounts'
 headers = {'Authorization': 'Basic bTJtOjEyMzQ1Ng=='}

 resp = requests.get(url=url, headers=headers)
 data = resp.json()
 print(data)
    
# result data
# [{u'username': u'jonhdoe', u'_id': u'Hkm672j-z', u'email': u'info@jonhdoe.com', u'details': {u'phone': u'', u'company': u'', u'name': u''}}]
```

## User accounts

There are two types of users, the main user who has access to all information and can view/create/edit/delete content and the member users, who only have read access to some data.

The main user is the general owner of the devices and the account, and there is only 1 per company. This user can create members and groups, and assign devices to them.

A main user can only add "member" accounts, he/she cannot add more main accounts.

## Categorías

| Categoría  | Descripción                                                  | Endpoint     |
| ---------- | ------------------------------------------------------------ | ------------ |
| `Accounts` | Allows you to manage/edit the main account and add members. It is not possible to delete the main account. | /v1/accounts |
| `Devices`  | It contains the devices. Each device stores the last data transmitted. | /v1/devices  |
| `Sensing`  | Device data history.                                         | /v1/sensing  |
| ` Groups`  | Creation of groups for members and devices. By creating a group your members will have read access to devices that have also been added to that group. | /v1/groups   |
| `Alerts`   | Stores the generated alerts. An alert is generated when a preset rule is met. | /v1/alerts   |
| `Rules`    | Allows you to create alert rules. For example if a battery is low or if the temperature is too high. The generated alerts are stored in `Alerts`. | /v1/rules    |
| `Commands` | Allows operational commands to be sent to devices.           | /v1/commands |

## Examples of Requests

In the Api documentation you can see all the possible requests, here are some of the most outstanding ones:

### Accounts

In the previous examples we have used the `/v1/accounts` endpoint which returned an array with a single result.

#### Obtain main account

```javascript
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/account
-> {"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}
```

In this case the endpoint is `/v1/account` (singular) and note the detail that unlike before with `/v1/accounts` now it does not return an array, but only a json object.

#### Create a Member with jQuery

We make a `POST` request and send a JSON object of type `Account`. 

```javascript
/* Basic account object */
const member = {
    username: 'MemberName',
    email: 'member@company.com',
    password: '123456'
}

// Add "member" object to "data" and set type: 'POST'
jQuery.ajax({
    data: member,
    type: 'POST',
    dataType: 'json',
    url: 'https://api.m2msystemsource.com/v1/accounts',
    headers: {
      Authorization: 'Basic am9uaGRvZToxMjM0NTY='
    },
    success: function(resp) {
      console.log(resp)
    }
});
  
/* // success result
{
    "_id": "3kd251Kid",
    "email": "info@jonhdoe.com",
    "username": "MemberName",
    "role": "member"
}
*/
```

#### Update Member

In this case we make a `PUT` request to update the data. We also include in the endpoit the ID of the member we want to edit `/v1/accounts/3kd251Kid`:

```javascript
/* Basic account object */
const member = {
    username: 'NewMemberName',
    password: 'New123456Pass'
}

// Add "member" object to "data" and set type: 'PUT'
jQuery.ajax({
    data: member,
    type: 'PUT',
    dataType: 'json',
    url: 'https://api.m2msystemsource.com/v1/accounts/3kd251Kid',
    headers: {
      Authorization: 'Basic am9uaGRvZToxMjM0NTY='
    },
    success: function(resp) {
      console.log(resp)
    }
});
  
/* success result
{
    "ok": 1
}
*/
```

### Devices

#### Obtain a device

The `/v1/devices/392839213` endpoint will return a JSON object (not Array).

```javascript
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/devices/392839213

/*
{
  "_id": "392839213",
  "name": "Device Name"
  "sensing": {
    "data": {
      "vi": "24404",
      "vs": "4897",
      "vb": "4209"
    },
    "time": 1614709052247
  }
}
*/
```

The `sensing` field contains the last data transmitted by the device:

| Property     | Description                                                  |
| ------------ | ------------------------------------------------------------ |
| sensing.time | Time in unix timestamp format at which data was received (GMT+0) |
| sensing.data | Object with the data received on the last connection. A Rhomb.io device can transmit many types of data depending on how it is configured. Consult our technical support if you have any doubts. |

#### Obtain a list of devices

Returns an array with JSON Objects:

```bash
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/devices

/*
{
  "_id": "392839213",
  "name": "Device Name"
  "sensing": {
    "data": {
      "vi": "24404",
      "vs": "4897",
      "vb": "4209"
    },
    "time": 1614709052247
  }
}
*/
```

## Types of Responses

All Api requests will return a JSON object with the server response. HTTP response codes are used:

| Code      | Description                                                  |
| --------- | ------------------------------------------------------------ |
| 200       | The request is successful and a valid response is received. Some requests may return an empty array `[]` indicating that there are no results and this does not necessarily imply an error, it will still be a valid response with code 200. |
| 400       | Data validation error. It occurs mainly when we send data in POST/PUT requests, for example if an email has an incorrect format. |
| 401       | Incorrect User / Password (token).                           |
| 403       | The user/password is valid but permissions to access the resource are missing. |
| 418       | User access has been blocked. Happens if login failed more than 10 times in a row. |
| 500 y 502 | Internal service problems unrelated to the use of the Api.   |

Per the table above, any response code greater than 200 will be understood to be an error.
