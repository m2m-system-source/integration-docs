# Rhomb.io Integración de WebSocket

V1.0 / 02-03-2020 / jenguidanos@rhomb.io / Docs For Developers

[TOC]

## Introducción

El WebSocket (también llamado watcher, el que todo lo ve) permite recibir información en tiempo real, esto quiere decir que tal y como salen los datos del dispositivo y llegan al servidor automáticamente entran en el WebSocket, cuestión de segundos o centésimas.

Es muy útil para crear gráficas en tiempo real o avisar en aplicaciones Frontend acerca de acontecimientos que sucedan en la red.

## Uso

Utilizamos la librería Socket.io como en el siguiente ejemplo:

```html
<body>

[...]

<script async src="https://watcher.m2msystemsource.com/socket.io/socket.io.js"></script>
<script type="text/javascript">
// connect to websocket with AccountID 3kd251Kid
const socket = window.io.connect('https://watcher.m2msystemsource.com/3kd251Kid')
socket.on('connect', () => console.log('io-connect success'))

socket.on('sensing', (data) => {
    console.log('New Sensing', data)
})
</script>
</body>

<!--
New Sensing {
  "keepAlive": false,
  "_id": "9yqZgwET0",
  "_device": "392839213",
  "time": 1614789217465,
  "data": {
    "vb": "4209",
    "vs": "4897",
    "vi": "24145"
  }
}
-->
```

En el ejemplo anterior primero se ha cargado la librería `https://watcher.m2msystemsource.com/socket.io/socket.io.js` y luego se realiza una conexión al servidor con la URL `https://watcher.m2msystemsource.com/3kd251Kid`

La parte final de la URL `3kd251Kid` corresponde al ID de la cuenta de usuario principal, deberá ser modificada con tu ID de usuario.

El evento sensing `socket.on('sensing' [function(data)]);` sucederá cada vez que un dispositivo envíe un dato. Se puede ver un ejemplo del objeto `data` en los comentarios del código anterior.

## Tipos de eventos en el WebSocket

| event         | Descripción                                                  |
| ------------- | ------------------------------------------------------------ |
| `greetings`   | Sucede cuando el dispositivo se conecta por primera vez al socket tras una desconexión (login) |
| `sensing`     | Sucede cada vez que un dispositivo genera un nuevo mensaje   |
| `cmd-sent`    | Se ha enviado un comando al dispositivo y este ha confirmado su recepción (pero aun no lo ha procesado) |
| `ack`         | El dispositivo está informado acerca de si un comando ha sido procesado con éxito o no. |
| `alive`       | El dispositivo ha enviado una notificación de "keep-alive", no contiene ningún dato relevante pero hace saber que está activo y funcionando |
| `alert`       | Se ha generado una alerta, contiene los datos de la alerta   |
| `alert-retry` | Se ha generado una alerta repetida (previamente ha sucedido el evento alert con los mismos datos) |

