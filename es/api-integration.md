# Rhombio RestFul Api Integration

[TOC]

## Introducción

Rhomb.io ofrece acceso público a su servicio de Api para aquellos clientes que deseen realizar una integración de datos en su plataforma. Estos son los puntos clave de la Api:

* Se realizan peticiones HTTPs tipo GET/POST/PUT/DELETE para acceder a los recursos.
* Siempre se utilizan conexiones seguras HTTPS.
* La autenticación se realiza mediante HTTP Basic y se requiere que todas las peticiones incluyan en el header el token de acceso.

## Token de Acceso

Se utiliza el nombre de usuario y la contraseña del servicio, separadas con dos puntos, para obtener una cadena codificada en base 64. Por ejemplo, si el nombre de usuario es `jonhdoe` y la contraseña `123456` podemos crear la siguiente cadena:

```text
jonhdoe:123456
```

La convertimos a Base64 con un servicio online https://www.base64encode.org y obtenemos:

```text
am9uaGRvZToxMjM0NTY=
```

Nuestro token es `am9uaGRvZToxMjM0NTY=` y lo añadiremos a todas las peticiones que se realicen.

> ¡IMPORTANTE! Este token da acceso total a los datos de la plataforma para editar o eliminar cualquier información. No lo debería utilizar en aplicaciones de navegador destinadas al público general. En su lugar puede usar la función de "miembros" para añadir usuarios con permisos restringidos.

## Primera Petición

Podemos realizar el ejemplo más básico utilizando la línea de comandos y cURL:

```bash
curl --user jonhdoe:123456 https://api.m2msystemsource.com/v1/accounts
-> [{"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}]
```

Mismo resultado con Token Base64

```bash
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/accounts
-> [{"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}]
```

### Ejemplo Javascript/jQuery

```javascript
jQuery.ajax({
  type: 'GET',
  dataType: 'json',
  url: 'https://api.m2msystemsource.com/v1/accounts',
  headers: {
    Authorization: 'Basic am9uaGRvZToxMjM0NTY='
  }
  success: function(resp) {
	console.log(resp)
  },
  error: function() {

  }
});

/* // success result
[
  {
    "_id": "Hkm672j-z",
    "email": "info@jonhdoe.com",
    "username": "jonhdoe",
    "details": {
      "name": "",
      "company": "",
      "phone": ""
    }
  }
]
*/
```

### Ejemplo Python/Requests

```python
import requests

 url = 'https://api.m2msystemsource.com/v1/accounts'
 headers = {'Authorization': 'Basic bTJtOjEyMzQ1Ng=='}

 resp = requests.get(url=url, headers=headers)
 data = resp.json()
 print(data)
    
# result data
# [{u'username': u'jonhdoe', u'_id': u'Hkm672j-z', u'email': u'info@jonhdoe.com', u'details': {u'phone': u'', u'company': u'', u'name': u''}}]
```

## Cuentas de usuario

Existen dos tipos de usuario, el usuario principal que tiene acceso a toda la información y puede ver/crear/editar/eliminar contenido y los usuarios miembro, que solo tiene acceso de lectura a algunos datos.

El usuario principal es el propietario general de los dispositivos y de la cuenta, y  solo existe 1 por por empresa. Este usuario puede crear miembros y grupos, y asignar dispositivos a estos.

Un usuario principal solo puede añadir cuentas de tipo "miembro", no puede añadir mas cuentas principales.

## Categorías

| Categoría  | Descripción                                                  | Endpoint     |
| ---------- | ------------------------------------------------------------ | ------------ |
| `Accounts` | Permite gestionar/editar la cuenta principal y añadir miembros. No es posible eliminar la cuenta principal. | /v1/accounts |
| `Devices`  | Contiene los dispositivos. Cada dispositivo almacena el último dato transmitido. | /v1/devices  |
| `Sensing`  | Históricos de datos de los dispositivos.                     | /v1/sensing  |
| ` Groups`  | Creación de grupos para miembros y dispositivos. Al crear un grupo sus miembros tendrán acceso de lectura a los dispositivos que también se hayan añadido a ese grupo. | /v1/groups   |
| `Alerts`   | Almacena las alertas generadas. Se genera una alerta cuando se cumple una regla preestablecida. | /v1/alerts   |
| `Rules`    | Permite crear reglas de alertas. Por ejemplo si una batería está baja o si la temperatura está muy alta. Las alertas generadas se almacenan en `Alerts`. | /v1/rules    |
| `Commands` | Permite enviar comandos operacionales a los dispositivos.    | /v1/commands |

## Ejemplos de Peticiones

En la documentación de la Api se pueden ver todas las peticiones posibles, vemos algunas de las más destacadas:

### Accounts

En los ejemplos anteriores hemos usado el endpoint `/v1/accounts` que nos ha devuelto un array con un único resultado.

#### Obtener cuenta principal

```javascript
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/account
-> {"_id":"Hkm672j-z","email":"info@jonhdoe.com","username":"jonhdoe","details":{"name":"","company":"","phone":""}}
```

En este caso el endpoint es `/v1/account` (en singular) y nota el detalle de que a diferencia de como ocurría antes con `/v1/accounts` ahora no se devuelve un array, sino solo un objeto json.

#### Crear un Miembro con jQuery

Realizamos una petición `POST` y enviamos un objeto JSON de tipo `Account`:

```javascript
/* Basic account object */
const member = {
    username: 'MemberName',
    email: 'member@company.com',
    password: '123456'
}

// Add "member" object to "data" and set type: 'POST'
jQuery.ajax({
    data: member,
    type: 'POST',
    dataType: 'json',
    url: 'https://api.m2msystemsource.com/v1/accounts',
    headers: {
      Authorization: 'Basic am9uaGRvZToxMjM0NTY='
    },
    success: function(resp) {
      console.log(resp)
    }
});
  
/* // success result
{
    "_id": "3kd251Kid",
    "email": "info@jonhdoe.com",
    "username": "MemberName",
    "role": "member"
}
*/
```

#### Actualizar Miembro

En este caso realizamos una petición `PUT` para actualizar los datos. Además incluimos en el endpoit la ID del miembro que queremos editar `/v1/accounts/3kd251Kid`:

```javascript
/* Basic account object */
const member = {
    username: 'NewMemberName',
    password: 'New123456Pass'
}

// Add "member" object to "data" and set type: 'PUT'
jQuery.ajax({
    data: member,
    type: 'PUT',
    dataType: 'json',
    url: 'https://api.m2msystemsource.com/v1/accounts/3kd251Kid',
    headers: {
      Authorization: 'Basic am9uaGRvZToxMjM0NTY='
    },
    success: function(resp) {
      console.log(resp)
    }
});
  
/* success result
{
    "ok": 1
}
*/
```

### Devices

#### Obtener un dispositivo

El endpoint `/v1/devices/392839213` devolverá un objeto JSON (no Array)

```javascript
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/devices/392839213

/*
{
  "_id": "392839213",
  "name": "Device Name"
  "sensing": {
    "data": {
      "vi": "24404",
      "vs": "4897",
      "vb": "4209"
    },
    "time": 1614709052247
  }
}
*/
```

El campo `sensing` contiene los últimos datos transmitidos por el dispositivo:

| Porpiedad    | Detalles                                                     |
| ------------ | ------------------------------------------------------------ |
| sensing.time | Hora en formato unix timestamp a la que se recibieron los datos (GMT+0). |
| sensing.data | Objeto con los datos recibidos en la ultima conexión. Un dispositivo Rhomb.io puede transmitir muchos tipos de datos según como se haya configurado. Consulta con nuestro soporte técnico si tienes dudas. |

#### Obtener un listado de dispositivos

Devuelve un array con Objetos JSON:

```bash
curl -H "Authorization: Basic am9uaGRvZToxMjM0NTY=" https://api.m2msystemsource.com/v1/devices

/*
{
  "_id": "392839213",
  "name": "Device Name"
  "sensing": {
    "data": {
      "vi": "24404",
      "vs": "4897",
      "vb": "4209"
    },
    "time": 1614709052247
  }
}
*/
```

## Tipos de Respuestas

Todas las peticiones Api devolverán un objeto JSON con la respuesta del servidor. Se utilizan códigos de respuesta HTTP:

| Código    | Descripción                                                  |
| --------- | ------------------------------------------------------------ |
| 200       | La petición es correcta y se recibe una respuesta válida. Algunas peticiones pueden devolver un array vacío `[]` indicando que no hay resultados y esto no implica necesariamente un error, seguirá siendo una respuesta válida con código 200. |
| 400       | Error de validación de datos. Se produce principalmente cuando enviamos datos en peticiones POST/PUT, por ejemplo si un email tiene un formato incorrecto. |
| 401       | Usuario / Contraseña incorrectos.                            |
| 403       | El usuario/contraseña es válido pero faltan permisos para acceder al recurso. |
| 418       | Se ha bloqueado el acceso al usuario. Sucede si se hace login fallido más de 10 veces seguidas. |
| 500 y 502 | Problemas internos del servicio ajenos al uso de la Api.     |

Por la tabla anterior, cualquier código de respuesta superior a 200 se entenderá que es un error
